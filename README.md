# Scripts

> This repo is defunct. I am putting the scripts back where they belong, which is with their respective repo's. Splitting them out wasn't a good idea. 

> These are scripts that I've written to wrap up other tools and provide scope/context management when needed. This is a work in progress and really in a single repository so I can share them across projects. The scripts are all in the `bin/` folder.

- [Scripts](#scripts)
    - [Wrapper Scripts](#wrapper-scripts)
        - [Script `faasteraws.sh`](#script-faasterawssh)
        - [Script `faasterraform.sh`](#script-faasterraformsh)
        - [Script `paasterraform.sh`](#script-paasterraformsh)
        - [How To Use](#how-to-use)
            - [Pre-conditions](#pre-conditions)
            - [Getting Ready](#getting-ready)
        - [Usage](#usage)

## Wrapper Scripts

The wrapper scripts provide a consisent way to support multiple credentials when dealing with AWS, either via the AWS CLI or `terraform`.

There are two `terraform` command wrappers, each is identical in terms of how they behave with one exception: they use different top-level namespace folders. If you're wondering why I don't use the `terraform workspace` commands, it's because I wanted different deployments/accounts which in turn use completely different AWS accounts, credentials etc. 

### Script `faasteraws.sh`

This script looks for `TARGET` folders within the `deployments/` namespace folder. 

```plain
Usage: ./bin/faasteraws.sh TARGET AWSSVC AWSCMD [AWSOPTIONS...]

    TARGET - name of configuration target in your deployments/ folder
             with the 'credentials' file used to source in env vars. Required.
    AWSSVC - The 'aws' service name per the AWS CLI reference
    AWSCMD - The service-specific sub-command, followed by its options etc.
```

### Script `faasterraform.sh`

This script looks for `TARGET` folders within the `deployments/` namespace folder. 

```plain
Usage: ./bin/faasterraform.sh TARGET TFCMD [TFOPTIONS...]

    TARGET - name of configuration target in your / folder
             with the 'credentials' file used to source in env vars. Required.
    TFCMD - The command for 'terraform' followed by its options. Supported
            commands are: init, graph, output, state, import, plan,
            apply, destroy
```

### Script `paasterraform.sh`

This script looks for `TARGET` folders within the `accounts/` namespace folder. 

```plain
Usage: ./bin/paasterraform.sh TARGET TFCMD [TFOPTIONS...]

    TARGET - name of configuration target in your / folder
             with the 'credentials' file used to source in env vars. Required.
    TFCMD - The command for 'terraform' followed by its options. Supported
            commands are: init, graph, output, state, import, plan,
            apply, destroy
```

### How To Use

#### Pre-conditions

1. You need to have an AWS account set up.
2. You need an API key set up, either as a credential for your root account or by setting up an IAM user with admin access.
3. You need the most recent version of [`terraform`](https://terraform.io) installed on your system when using `faasterraform.sh` and `paasterraform.sh`. 
    - I use `brew` (or `linuxbrew`) to install it and keep it up to date.
4. You need the most recent version of [`aws`](https://docs.aws.amazon.com/cli/latest/index.html) CLI installed on your system when using `faasteraws.sh`. 
    - I use `brew` (or `linuxbrew`) to install it and keep it up to date.
5. Clone this repo.

#### Getting Ready

1. In whichever projects where you plan to use the scripts from, create the following folders:
    - `accounts/` for use with `paasterraform.sh`
    - `deployments/` for use with `faasterraform.sh` and `faasteraws.sh`
2. In whichever namespace you plan to use, create a sub-folder for your target: e.g. `mydeployment/`. Within that account ...
3. Create a file called `credentials` and make sure it exports the following env variables:
    - `export AWS_ACCESS_KEY_ID=YOUR_ACCOUNT_ADMIN_ACCESS_KEY`
    - `export AWS_SECRET_ACCESS_KEY=YOUR_ACCOUNT_ADMIN_SECRET_KEY`
4. Create a file called `terraform.tfvars` and make sure it has the following properties:
    - `aws_region = YOUR_AWS_REGION_IF_NOT_US_EAST_1`
    - `deploy_name = "example"`

### Usage

These wrapper scripts are used to execute the `terraform` or `aws` commands within a particular namespace, so unlike calling them directly the first parameter is the target namespace and then the commands and their parameters.

```bash
$ ./bin/faasterraform mydeployment init
```

