#!/usr/bin/env bash
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

#
# The help output
#
HELP="

Usage: ${0} TARGET TFCMD [TFOPTIONS...]

    TARGET - name of configuration target in your $NAMESPACE/ folder
             with the 'credentials' file used to source in env vars. Required.
    TFCMD - The command for 'terraform' followed by its options. Supported 
            commands are: init, graph, output, state, import, plan, 
            apply, destroy

"

NAMESPACE=accounts
TARGETPATH=$NAMESPACE/$1

#
# Command line flag to set the "config_path" variable for the terraform config
# when using commands that process the current deployment target configuration
#
OPTCFGVAR="-var config_path=$TARGETPATH"

#
# Where to find the backend config file for "init" command
#
BACKENDCFGPATH="$TARGETPATH/backend.tfvars"
OPTCFGBACKEND="-backend-config=$BACKENDCFGPATH"
#
# Set the terraform data directory within the namespace target so we
# can keep that data separate between deployments
#
export TF_DATA_DIR="$TARGETPATH/terraform.datadir"

#
# Process the commands that we know
#
if [ -f "$TARGETPATH/credentials" ]; then
    source "$TARGETPATH/credentials"
    mkdir -p $TF_DATA_DIR
    case $2 in
    init)
        if [ -f "$BACKENDCFGPATH" ]; then
            terraform $2 $OPTCFGBACKEND \
                $3 $4 $5 $6
        else
            terraform $2 \
                $3 $4 $5 $6
        fi
        ;;
    graph)
        terraform $2 \
            $3 $4 $5 $6
        ;;
    output)
        terraform $2 \
            -state=$TARGETPATH/terraform.tfstate \
            $3 $4 $5 $6
        ;;
    state)
        case $3 in
        push)
            terraform $2 \
                $3 $4 $5 $6
            ;;
        *)
            terraform $2 $3 \
                -state=$TARGETPATH/terraform.tfstate \
                $4 $5 $6
            ;;
        esac
        ;;
    import|plan|apply|destroy)
        terraform $2 $OPTCFGVAR \
            -state=$TARGETPATH/terraform.tfstate \
            -var-file=$TARGETPATH/terraform.tfvars \
            $3 $4 $5 $6
        ;;
    "")
        echo "ERR: Terraform command required."
        echo "$HELP"
        ;;
    *)
        echo "ERR: Unsupported command '$2', update the script to handle this."
        ;;
    esac
else
    echo "ERR: Specify name of configuration target in your $NAMESPACE/ folder."
    echo "$HELP"
fi

