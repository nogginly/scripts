# Scripts Changelog

The latest version of this file can be found at the master branch of the `nogginly/scripts` repository.

## 0.2.0

- Adding `faasteraws.sh` wrapper script for AWS CLI to the repo.
- Modified scripts to use `#!/usr/bin/env bash` to run the script. See [pure-bash-bible](https://github.com/dylanaraps/pure-bash-bible) for why and soooo much more.

## 0.1.0

- Moving common helper scripts `faasterraform.sh` and `paasterraform.sh` under this one repository.
